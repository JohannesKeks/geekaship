describe("Exercise 02", function() {

  describe("Strings", function() {
    it("should be equal regardless of quotes used", function() {
      expect('Some String').toEqual("Some String");
    });

    it("use typeof() to check the type", function() {
      expect(typeof("this string")).toEqual('string');
    });

    it("use String() to convert 2 to '2'", function() {
      expect(String(2)).toEqual("2");
    });
  });

  describe("Numbers", function() {
    it("small integers should be numbers", function() {
      expect(typeof(100)).toEqual('number');
    });

    it("big integers should be ___", function() {
      expect(typeof(1000000000000)).toEqual('number');
    });

    it("3.141 should be ___", function() {
      expect(typeof(3.141)).toEqual('number');
    });

    it("should output ___ when ___", function() {
      expect(10/0).toEqual(Infinity);
    });

    it("should output ___ when ___", function() {
      expect(-10/0).toEqual(-Infinity);
    });

    it("use Number() to convert strings to numbers", function() {
      expect(Number("123")).toEqual(123);
      expect(Number("1234567890123456")).toEqual(1234567890123456);
      expect(Number("3.141")).toEqual(3.141);
    });

  });

  describe("Booleans", function() {
    it("is true", function () {
      expect(true).toBeTruthy();
      expect( 1 == 1 ).toBeTruthy();
      expect( "a" == 'a' ).toBeTruthy();
      expect( null == null ).toBeTruthy();
    });

    it("is false", function () {
      expect(false).toBeFalsy();
      expect( 1 == 2 ).toBeFalsy();
      expect( "a" == "b" ).toBeFalsy();
      expect( null == "" ).toBeFalsy();
    });

  });
});
