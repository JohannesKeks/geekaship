describe("Exercise 11", function() {

  it ("#countUpperCaseA", function() {
    function countUpperCaseA(cs) {
      ___;
    }

    expect(countUpperCaseA("Apples and Ants")).toEqual(2);
  });

  it ("#countUpperCaseChars", function() {
    function countUpperCaseChars(cs) {
      ___;
    }

    expect(countUpperCaseChars("Apples and Ants", "A")).toEqual(2);
  });

  it ("#makeCharCounter", function() {
    function makeCharCounter(letter) {
      return function(cs) {
        ___;
      }
    }

    var countA = makeCharCounter("A");
    expect(countA("Apples and Ants")).toEqual(2);
  });

});