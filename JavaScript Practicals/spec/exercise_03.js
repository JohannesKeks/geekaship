describe("Exercise 03", function() {

  it("does some weird stuff", function() {
    expect("0" == '0').toBeTruthy();
    expect("" == '').toBeTruthy();
  });

  it("does some really weird stuff", function() {
    expect("bacon" == false).toBeFalsy();
    expect("bacon" == true).toBeFalsy();
  });

  it("has null as a type, and behaves weirdly for type coercion", function() {
    expect(null).toEqual(null);
    expect(5 + null).toEqual(5);
    expect(5 * null).toEqual(0);
    expect("5" + null).toEqual('5null');
  });

});
