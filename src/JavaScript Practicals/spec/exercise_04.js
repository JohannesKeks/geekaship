describe("Exercise 04", function() {

  it("NaN is Not a Number, and it behaves weird-ish", function() {
    expect("5" + 2).toEqual('52');
    expect("5" - 2).toEqual(3);
    expect("5" * 2).toEqual(10);
    expect("five" + 2).toEqual('five2');
    expect("five" - 2).toEqual(NaN);
    expect("five" * 2).toEqual(NaN);
  });

  it("there is undefined too, but we can treat it as a null but its not null", function() {
    expect(undefined).toEqual(undefined);
    expect(undefined == null).to;
    expect(5 + undefined).toEqual(NaN);
    expect(5 * undefined).toEqual(NaN);
    expect("five" + undefined).toEqual("fiveundefined");
  });

  describe("Some rules", function() {
    it("undefined => ___", function() {
      expect(undefined).toBeFalsy();
    });

    it("null => ___", function() {
      expect(null).toBeFalsy();
    });

    it("number is ___", function() {
      expect(+0).toBeFalsy();
      expect(-0).toBeFalsy();
      expect(NaN).toBeFalsy();
      expect(0).toBeFalsy();
    });


    it("string is ___", function() {
      expect("").toBeFalsy();
      expect("abc").toBeTruthy();
    });

    it("an object is ___", function() {
      expect({}).toBeTruthy();
      expect(12).toBeTruthy();
    });

  });

});
