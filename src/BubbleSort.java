import java.util.Scanner;

public class BubbleSort
{
    public static void main (String[] args){

        int n, d, c, swap;

        Scanner in = new Scanner(System.in);


        System.out.println("Input the number of integers to sort");
        n = in.nextInt();

        int SortArray[] = new int[n];

        System.out.println("Enter " + n + " Integers");

        for(c = 0; c < n; c++) {
            SortArray[c] = in.nextInt();


        }


        for(c = 0; c < n; c++){
            for(d = 0; d < n - c - 1;d++){
                if (SortArray[d] > SortArray[d+1]){

                    swap = SortArray[d];
                    SortArray[d] = SortArray[d + 1];
                    SortArray[d+1] = swap;

                }
            }
        }


        System.out.println("Sorted numbered list");
        System.out.println("====================");

        for(c = 0; c < n; c++  ){

            System.out.println(SortArray[c]);
        }

    }
}
