// We are using Jasmine test library

// For now, just follow this layout.
// Don't think about how it works.
// Assume it is magic!

describe("Exercise 01", function() {

  it ("should add 1 + 2", function() {
    expect(1 + 2).toEqual(3);
  });

  it ("should add 2 + 2", function() {
    expect(2 + 2).toEqual(4);
  });

});