describe("Exercise 10", function() {
  it("just a duck", function() {
    var duck = {};

    duck.speak = function(s) { return "The duck quacked: " + s;}

    expect(duck.speak("meh")).toEqual("The duck quacked: meh");
  });

  it("animals that speak", function() {
    function speak(s) {
      return "The " + s.type + " said: " + s.words;
    }

    var duck = {type: "duck", words:"quack"};
    expect(speak(duck)).toEqual("The duck said: quack");

    var dog = {type: "dog", words:"woof"};
    expect(speak(dog)).toEqual("The dog said: woof");
  });

  it("functions are objects that can be instantiated", function() {
    function Animal(type) {
      this.type = type;
      this.speak = function(s) { return "The " + this.type + " said: " + s; }
    }

    var dog = new Animal("dog");

    expect(dog.type).toEqual('dog');
    expect(dog.speak("woof")).toEqual(dog);

    // make a cat meow;
    var cat = new Animal("cat");
    expect(cat.speak()).toEqual("The cat said: meow");
  });

  it("the prototype object", function() {

    function Animal(type) {
      this.type =  type;
    }

    Animal.prototype.speak = function(s) {
      return "The " + this.type + " said: " + s;
    }

    var dog = new Animal("dog");
    expect(dog.speak()).toEqual("The dog said: woof");

    var cat = new Animal("cat");
    Animal.___.loves = function(s) { return "The " + this.type + " loves " + s; }
    expect(cat.___).toEqual("The cat loves itself");

    // can a dog love its owner?
    expect(___).toEqual("The dog loves owner");
  });

  it("overriding properties and functions", function() {
    function Animal(type) {
      this.type =  type;
    }

    Animal.prototype.diet = "everything";
    Animal.prototype.speak = function(s) { return "The " + this.type + " said: " + s; }

    var human = new Animal("human");
    expect(human.diet).toEqual("everything");

    var cow = new Animal("cow");
    cow.diet = "grass";
    expect(cow.diet).toEqual('grass');

    cow.speak = ___;
    expect(cow.speak()).toEqual("moo");
  });

  it("this and that", function() {
    function Animal(type,where) {
      var ___ = ___;
      this.type =  type;
      this.isIn = {};
      this.locations = where;
      where.forEach( function(loc) {
        ___.isIn[loc] = true;
      });
    }

    var human = new Animal("human", ["Africa", "Asia", "Europe"]);
    expect(human.locations).toEqual(___);
    expect(human.isIn.Africa).___();
  });

});