describe("Exercise 05", function() {

  describe("Comparison with == (type coercion in play)", function() {
    it("null == undefined => ___", function() {
      expect(null == undefined).toBeTruthy();
    });

    it("number == string", function() {
      expect(11 == "11").toBeTruthy();
    });

    it("string == number", function() {
      expect("33" == 33).toBeTruthy();
    });

    it("empty string == number", function() {
      expect("0" == 0).toBeTruthy();
    });

    it("(any) == boolean", function() {
      expect(1 == true).toBeTruthy();
      expect(0 == true).toBeFalsy();
      expect(-1 == true).toBeFalsy();
      expect(5 == true).toBeFalsy();
      expect("1" == true).toBeTruthy();
      expect("0" == true).toBeFalsy();
      expect("-1" == true).toBeFalsy();
      expect("5" == true).toBeFalsy();
    });




  });
});

