var isHtml5Compatible = document.createElement('canvas').getContext != undefined;
//var selectedSurvey = document.getElementById("selectpicker").value;

        if (isHtml5Compatible) {
            initiateLocalStorage();
            }

        function initiateLocalStorage() {
            // Create the AngularJS app
            var app = angular.module('surveys', ['storageService']);

            // Create the Controller
            app.controller('surveyController', ['$scope', 'getLocalStorage', function ($scope, getLocalStorage) {

                //Read the surveys from storage

                $scope.surveys = getLocalStorage.getSurvey();


                //Add survey - using AngularJS push to add survey in the survey Object
                //Call Update survey to update the locally stored survey List

                $scope.addSurvey = function () {
                    if($scope.surveyName.length > 0 && $scope.description.length > 0 && $scope.timeFrame.length > 0 )
                    {
                        $scope.surveys.push({ 'surveyName': $scope.surveyName, 'description': $scope.description, 'timeFrame': $scope.timeFrame });
                                            getLocalStorage.updateSurvey($scope.surveys);
                                            $scope.surveyName = '';
                                            $scope.description = '';
                                            $scope.timeFrame = '';

                    }






                };



                //Delete survey - Using AngularJS splice to remove the surv row from the survey list
                //All the Update survey to update the locally stored survey List
                //Update the Count
                $scope.deleteSurvey = function (surv) {
                    $scope.surveys.splice($scope.surveys.indexOf(surv), 1);
                    getLocalStorage.updateSurvey($scope.surveys);

                };
            }]);

            //Create the Storage Service Module
            //Create getLocalStorage service to access UpdateSurvey and getSurvey method

            var storageService = angular.module('storageService', []);
            storageService.factory('getLocalStorage', function () {

                var surveyList = {};
                var instructionList ={};
                return {
                    list: surveyList, list:instructionList,

                    updateSurvey: function (SurveysArr) {
                        if (window.localStorage && SurveysArr) {
                            //Local Storage to add Data
                            localStorage.setItem("surveys", angular.toJson(SurveysArr));
                        }
                        surveyList = SurveysArr;

                    },
                    getSurvey: function () {
                        //Get data from Local Storage
                        surveyList = angular.fromJson(localStorage.getItem("surveys"));
                        return surveyList ? surveyList : [];
                    }
                };


            });


        }





