
import java.util.Scanner;
import javax.swing.*;

public class EvenOrOdd
{
    public static void main(String[] args) {
        int number;
        String operator;
        String newString1 = "mod";
        String newString2 = "bitwise";

        operator = JOptionPane.showInputDialog(null, "Choose the operator you would like to use");

        //convert user input
        number = Integer.parseInt(JOptionPane.showInputDialog(null, "Enter the number to be tested"));

        //finding an even or odd number using modulo operator
        if (operator.equalsIgnoreCase(newString1)) {

            if (number % 2 != 0) {
                JOptionPane.showMessageDialog(null, number + " is an odd number");


            } else {
                JOptionPane.showMessageDialog(null, number + " is an even number ");
            }
            JOptionPane.showMessageDialog(null, "And you used a modulus operator");

        }
        //finding an even or odd number using bitwise method

        else if (operator.equalsIgnoreCase(newString2)) {

            int res = number & 1;

            if (res == 0) {
                JOptionPane.showMessageDialog(null, number + " is Even Number");
            } else {
                JOptionPane.showMessageDialog(null, number + " is an odd Number");
            }
            JOptionPane.showMessageDialog(null, "And you used bitwise operator");

        }
    }
}
