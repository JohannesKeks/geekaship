 // Create the AngularJS app for instructions
            var  = angular.module('instructions', ['storageService']);
            //controller
            app.Controller('instructionController' 'getLocalStorage',function($scope,getLocalStorage){

                //reading the instructions
                $scope.instructions = getLocalStorage.getInstructions();

                //adding the instructions
                $scope.addInstruction = function()
                {
                    $scope.instructions.push({'selectpicker':$scope.selectpicker,'description':$scope.description,'instructionOrder':$scope.instructionOrder});
                    getLocalStorage.updateInstruction($scope.instructions);
                    $scope.selectpicker = '';
                    $scope.description = '';
                    $scope.instructionOrder = '';
                };

                //deleting the instruction
                $scope.deleteInstruction = function(inst){
                    $scope.instructions.splice($scope.instructions.indexOf(inst),1);
                    getLocalStorage.updateInstruction($scope.instructions);
                }

                var storageService = angular.module('storageService', []);
                            storageService.factory('getLocalStorage', function () {
                                //creating arrays to hold revelant info
                                var InstructionList = {};
                                return {
                                    list: InstructionList,

                                    updateSurvey: function (instructionsArr) {
                                        if (window.localStorage && instructionsArr) {
                                            //Local Storage to add Data
                                            localStorage.setItem("surveys", angular.toJson(instructionsArr));
                                        }
                                        InstructionList = instructionsArr;

                                    },
                                    getSurvey: function () {
                                        //Get data from Local Storage
                                        surveyList = angular.fromJson(localStorage.getItem("instructions"));
                                        return InstructionList ? InstructionList : [];
                                    }
                                };
                            });
            });