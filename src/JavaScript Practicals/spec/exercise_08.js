describe("Exercise 08", function() {

  it ("named functions", function() {
    function add(a,b) { return a + b; }
    expect(add(1,2)).toEqual(3);
  });

  it ("missing args, too many args", function() {
    function f(a,b) { return {_a: a, _b: b}; }
    var z = f(1,2);
    expect(z._a).toEqual(1);
    expect(z._b).toEqual(2);

    var z = f(1);
    expect(z._a).toEqual(1);
    expect(z._b).toEqual(undefined);

    var z = f(1,2,3);
    expect(z._a).toEqual(1);
    expect(z._b).toEqual(2);
  });


  it ("varargs", function() {

    function f() {
      var args = [];
      for(var i = 0; i < arguments.length; i++) {
        args.push(arguments[i] * 2);
      }
      return args;
    }

    var z = f(1,2,3,4);
    expect(z).toEqual([ 2, 4, 6, 8 ]);
  });


  it ("functions as values", function() {
    var add = function(a,b) { return a + b; }
    expect(3).toEqual(3);
  });

  it ("anonymous functions", function() {
    expect(3).toEqual( function() { return 3;}());
    expect(3).toEqual( function(a,b) { return 1+2;}(1,2));
  });

  it ("passing a function into a function", function() {
    var add = function(a,b) { return a + b; }
    var mul = function(a,b) { return a * b; }
    var calculate = function(a,b, op) { return op(a,b); }
    expect(calculate(1,2, mul)).toEqual(2);
    expect(calculate(3,2, mul)).toEqual(6);
  });

  it ("returning a function from a function", function() {
    var makeAdd = function() {
      return function(a,b) { return a + b; };
    }
    var add = makeAdd(3);
    expect(makeAdd(1,2)).toEqual(3);
  });

  it ("a closure", function() {
    var incrementer = function(value) {
      return function(n) { return n + value; };
    }
    var add1 = incrementer(1);
    expect(add1(2)).toEqual(3);

    // make an incrementer that increments by 5
    var ___ = ___;
    expect(___).toEqual(___);
  });

});
