describe("Exercise 09", function() {
  it ("scope pollution", function() {
    var months = ["January", "February", "March"];

    function monthName(n) { return months[n]; }

    expect(monthName(1)).toEqual("February");
  });

  it ("don't bleed scope", function() {

    var monthName = function() {
      var months = ["January", "February", "March"];
      return months;
    }();

    expect(monthName[1]).toEqual("February");
  });

  it ("functions as a namespace", function() {

    var months = function() {
      var months = ["January", "February", "March"];
      return {name: function(n) {return months[n-1];}, number: function(m) {return months.indexOf(m)+1;}};;
    }();

    expect(months.name(2)).toEqual("February");
    expect(months.number("February")).toEqual(2);
  });
});