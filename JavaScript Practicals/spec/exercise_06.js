describe("Exercise 06", function() {

  it ("should be zero indexed", function() {
    var ns = [1,2,3];
    expect(ns[0]).toEqual(1);
  });

  it ("#length", function() {
    expect([1,2,3,4,5].length).toEqual(5);
  });

  it ("#push", function() {
    var ns = [2,4,6,8];
    var newLength = ns.push(10);
    expect(newLength).toEqual(5);
    expect(ns).toContain(4);
  });

  it ("#pop", function() {
    var ns = [2,4,6,8];
    var value = ns.pop();
    expect(ns.length).toEqual(3);
    expect(value).toEqual(8);
  });

  it ("#shift", function() {
    var ns = [2,4,6,8];
    var value = ns.shift();
    expect(ns).toEqual([ 4, 6, 8 ]);
    expect(value).toEqual(2);
  });

  it ("#unshift", function() {
    var ns = [2,4,6,8];
    var value = ns.unshift(10);
    expect(ns).toEqual([10,2,4,6,8]);
    expect(value).toEqual(5);
  });

  it ("#concat", function() {
    var ns = [2,4,6,8];
    var result = ns.concat(["a","b","c", "d"]);
    expect(result).toEqual([ 2, 4, 6, 8, 'a', 'b', 'c', 'd' ]);
  });

  it ("#reverse and #sort", function() {
    var ns = [6,2,"b",8,4,"a"];
    expect(ns.reverse()).toEqual(['a',4,8,'b',2,6] );
    expect(ns.sort()).toEqual([2,4,6,8,'a','b']);
  });

  it ("#join", function() {
    var ns = [1,3,5];
    expect(ns.join()).toEqual('1,3,5');
    expect(ns.___).toEqual(undefined);
    expect(ns.join(">>")).toEqual('1>>3>>5');
  });

  it ("#slice", function() {
    var ns = [1,3,5,7,9,11,13];
    expect(ns.slice(2,5)).toEqual( [ 5, 7, 9 ]);
    expect(ns.slice(4)).toEqual([ 9, 11, 13 ]);
    expect(ns.slice(-2)).toEqual([ 11, 13 ]);
  });

  it ("#splice part 1", function() {
    var ns = [1,3,5,7,9,11,13];
    var result = ns.splice(3);
    expect(ns).toEqual([ 1, 3, 5 ]);
    expect(result).toEqual([ 7, 9, 11, 13 ]);
  });

  it ("#splice part 2", function() {
    var ns = [1,3,5,7,9,11,13];
    var result = ns.splice(2,4);
    expect(ns).toEqual([ 1, 3, 13 ]);
    expect(result).toEqual([ 5, 7, 9, 11 ]);
  });

});